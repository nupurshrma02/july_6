//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, Image } from 'react-native';
import { TextInput } from 'react-native-gesture-handler';

// create a component
class SetAircraft extends Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={{padding: 50, backgroundColor: '#fff', width:'100%', alignItems:'center'}}>
                    <Image source={require('../images/2.png')} 
                         style={{height:70, width:70}}/>
                </View>
            {/* Aircraft */}

            <Text style={styles.text}>Aircraft</Text>
                <View style={styles.fields}>
                    <Text> Aircraft Type</Text>
                    <TextInput placeholder='Aircraft Type' style={{marginLeft: 180, marginTop: -10}}/>
                </View>
                <View style={styles.fields}>
                    <Text> Aircraft ID</Text>
                    <TextInput placeholder='Aircraft ID' style={{marginLeft: 200, marginTop: -10}}/>
                </View>
            {/* Category */}

            <Text style={styles.text}>Category</Text>
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        //justifyContent: 'center',
        //alignItems: 'center',
        //backgroundColor: '#2c3e50',
    },
    text: {
      fontSize: 15,
      color: 'grey',
      fontWeight: 'bold',
      marginTop: 10,
      marginLeft: 10,
      marginBottom: 5,
    },
    fields:{
      padding: 5,
      backgroundColor: '#fff',
      borderBottomColor: '#000',
      borderBottomWidth: 1,
      flexDirection:'row',
    },
});

//make this component available to the app
export default SetAircraft;
