//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';

// create a component
class SettingScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {Selectable: true};
      }
    
    render() {
        return (
            <View style={styles.container}>
                <TouchableOpacity onPress={()=> this.props.navigation.navigate('Display')}><Text style={styles.text}>Display/Default</Text></TouchableOpacity>
                <TouchableOpacity><Text style={styles.text}>Profile Update</Text></TouchableOpacity>
                <TouchableOpacity><Text style={styles.text}>Build LogBook</Text></TouchableOpacity>
                <TouchableOpacity><Text style={styles.text}>Gallery</Text></TouchableOpacity>
                <TouchableOpacity><Text style={styles.text}>Support/Backup</Text></TouchableOpacity>
                <TouchableOpacity><Text style={styles.text}>Help Videos</Text></TouchableOpacity>
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        //backgroundColor: '#2c3e50',
    },
    text: {
        fontSize: 25,
    },

});

//make this component available to the app
export default SettingScreen;
